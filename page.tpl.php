<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print strip_tags(str_replace("<br>"," ",$head_title)) ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
include("page.tpl.php.inc");
print roundness_border_part1($directory,"id=\"header\" class=\"roundblock_outer_spacing\"");
/***** END:   Roundness Theme Code inserted here ************/
?>
  <table border="0" cellpadding="0" cellspacing="0" id="header">
    <tr><td id="logo" rowspan="3">
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    </td>
    <td width="40%">
      <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
    </td>
    <td width="60%">
      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
    </td>
    <td id="menu">
       <?php print $search_box?>
    </td></tr>
    <tr><td colspan="3" id="menu">
      <?php if (isset($secondary_links)) { ?><div id="secondary"><?php print theme('links', $secondary_links) ?></div><?php } ?>
      <?php if (isset($primary_links)) { ?><div id="primary"><?php print theme('links', $primary_links) ?></div><?php } ?>
    </td></tr>
    <tr><td colspan="3"><div><?php print $header ?></div>
    </td></tr>
  </table>
<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part2($directory);
/***** END:   Roundness Theme Code inserted here ************/
?>

<table cellpadding="0" cellspacing="0" class="roundblock_outer_spacing">
  <tr>
    <?php if ($sidebar_left) { ?><td id="sidebar-left">
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part1($directory,"id=\"sidebar-left-color\"");
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
      <?php print $sidebar_left ?>
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part2($directory);
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
    </td><?php } ?>
    <td valign="top">
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part1($directory,"id=\"cbar-color\"");
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
      <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>      <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php print $help ?>
        <?php print $messages ?>
        <?php print $content; ?>
      </div>
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part2($directory);
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
    </td>
    <?php if ($sidebar_right) { ?><td id="sidebar-right">
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part1($directory,"id=\"sidebar-right-color\"");
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
      <?php print $sidebar_right ?>
      <?php
      /***** BEGIN: Roundness Theme Code inserted here ************/
      print roundness_border_part2($directory);
      /***** END:   Roundness Theme Code inserted here ************/
      ?>
    </td><?php } ?>
  </tr>
</table>

<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part1($directory,"id=\"footer-color\" class=\"roundblock_outer_spacing\"");
/***** END:   Roundness Theme Code inserted here ************/
?>
<div id="footer">
  <?php print $footer_message ?>
</div>
<?php
/***** BEGIN: Roundness Theme Code inserted here ************/
print roundness_border_part2($directory);
/***** END:   Roundness Theme Code inserted here ************/
?>
<?php print $closure ?>
</body>
</html>
