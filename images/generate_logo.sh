#!/bin/bash

header_color=$1
lsb_color=$2
rsb_color=$3
cbar_color=$4
footer_color=$5

#Header Images
convert -size 80x80 xc:white -fill "${header_color}" -stroke "${header_color}" -draw "roundrectangle 2,2,77,16 2,2" logo.png
mv logo.png temp.png
convert temp.png -fill "${lsb_color}" -stroke "${lsb_color}" -draw "roundrectangle 2,20,14,60 2,2" logo.png
mv logo.png temp.png
convert temp.png -fill "${rsb_color}" -stroke "${rsb_color}" -draw "roundrectangle 65,20,77,45 2,2" logo.png
mv logo.png temp.png
convert temp.png -fill "${cbar_color}" -stroke "${cbar_color}" -draw "roundrectangle 18,20,61,66 2,2" logo.png
mv logo.png temp.png
convert temp.png -fill "${footer_color}" -stroke "${footer_color}" -draw "roundrectangle 2,70,77,73 2,2" logo.png
rm temp.png
convert logo.png -resize 32 logo32x32.png
convert logo.png -resize 16 logo16x16.png
png2ico favicon.ico logo16x16.png logo32x32.png
rm logo16x16.png
rm logo32x32.png
mv logo.png ..
mv favicon.ico ..
