<?php
/*
This file contains two routines which are needed for the roundness theme.
These functions are called from 'php.tpl.php' and are used to build a 3x3 table surrounding the header region as wells as the side bars, central region and the footer.  The 3x3 table is rendered in such a way that the outer 8 cells form an border/edge with rounded corners. The central cell contains the header (and other) block(s)
*/
?>


<?php
/*
 $basep = $directory which seems not to be global and is therefore     undefined inside the function
 $tablespecification is intended to contain id and class statements needed for a particular table
*/
function roundness_border_part1($basep,$tablespecification) {
  return "<! Start of Roundness Border Table surrounding actual display region >
  <TABLE border=\"0\" cellpadding=\"0\" cellspacing=\"0\" $tablespecification>
    <tr><td>
        <IMG src=\"/$basep"."/images/corner_ul.gif\" class=\"image_corner\">
      </td>
      <td class=\"row_height\"></td>
      <td>
        <IMG src=\"/$basep"."/images/corner_ur.gif\" class=\"image_corner\">
    </td></tr>
    <tr>
      <td class=\"edge_width\"></td>
      <td width=\"100%\">
<! End of section one of Roundness Border Table>";
}
?>

<?php
/*
 $basep = $directory which seems not to be global and is therefore     undefined inside the function
*/
function roundness_border_part2($basep) {
  return "<! Start of Roundness Border Table section two >
      </td>
    <td  class=\"edge_width\"></td></tr>
    <tr >
      <td>
        <IMG src=\"/$basep"."/images/corner_ll.gif\" class=\"image_corner\">
      </td>
      <td class=\"row_height\"></td>
      <td>
        <IMG src=\"/$basep"."/images/corner_lr.gif\" class=\"image_corner\">
    </td></tr>
  </TABLE>
<! End of Roundness Border Table surrounding actual display region >";
}
?>