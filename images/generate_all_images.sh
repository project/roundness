#!/bin/bash

# -------------------
# Edit these values to change the colos of the logo and favicon image and the corner radius
# lsb - left-sidebar
# rsb - right-sidebar
# cbar - central bar/region (i.e. main content area)
corner_radius="10"
header_color="#1F94DC"
lsb_color="#F79F6C"
rsb_color="#BDDD8D"
cbar_color="#f0f0f0"
footer_color="#B3B3B3"
body_background="#ffffff"
# ---------------------

imagedir=`pwd`
${imagedir}/generate_corners.sh $corner_radius $body_background
${imagedir}/generate_logo.sh $header_color $lsb_color $rsb_color $cbar_color $footer_color
