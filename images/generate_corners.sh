#!/bin/bash

# For some reason the convert program will not create transparant ares in images less than 20px
# Therefore a bigger image is created first and then it is resized

block_size=$1

convert -colors 2 -size 50x50 xc:"$2" -fill "#010101" -stroke "#010101" -draw "circle 49,49 49,0" corner_ul.gif
convert corner_ul.gif -transparent "#010101" temp.gif
convert temp.gif -resize $block_size corner_ul.gif
rm temp.gif
convert corner_ul.gif -flop corner_ur.gif
convert corner_ur.gif -flip corner_lr.gif
convert corner_lr.gif -flop corner_ll.gif
